<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function webmanifest_insert_head($flux) {
	$flux .= "\n" . '<link rel="manifest" href="'.generer_url_public('site.webmanifest').'" />';
	
	return $flux;
}
