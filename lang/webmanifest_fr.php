<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/verifier.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'configurer_background_color_label' => 'Couleur de fond',
	'configurer_description_label' => 'Description longue',
	'configurer_display_choix_browser_label' => 'Navigateur',
	'configurer_display_choix_fullscreen_label' => 'Plein écran',
	'configurer_display_choix_minimal-ui_label' => 'Minimal',
	'configurer_display_choix_standalone_label' => 'Standalone',
	'configurer_display_explication' => 'Pour en savoir plus sur les différents modes, se référer à <a href="https://developer.mozilla.org/fr/docs/Web/Manifest#display">Manifeste des applications web sur developer.mozilla.org</a>',
	'configurer_display_label' => 'Mode d’affichage',
	'configurer_icon_explication' => 'Privilégiez une icône carrée, d\'au moins 512px de côté',
	'configurer_icon_label' => 'Icône',
	'configurer_name_label' => 'Nom complet',
	'configurer_short_name_label' => 'Nom court',
	'configurer_start_url_label' => 'URL de démarrage',
	'configurer_theme_color_label' => 'Couleur du thème',
);
