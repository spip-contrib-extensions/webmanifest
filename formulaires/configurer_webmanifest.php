<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_configurer_webmanifest_saisies_dist() {
	include_spip('inc/filtres');
	
	$saisies = array(
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'name',
				'label' => _T('webmanifest:configurer_name_label'),
				'obligatoire' => 'oui',
				'defaut' => $GLOBALS['meta']['nom_site'],
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'icon',
				'type' => 'file',
				'label' => _T('webmanifest:configurer_icon_label'),
				'explication' => _T('webmanifest:configurer_icon_explication'),
				'inserer_debut' => ($chemin = lire_config('webmanifest/icon')) ? inserer_attribut(filtrer('balise_img', _DIR_IMG.$chemin, '', '', '200'), 'style', 'max-width:200px;height:auto;') : '',
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'short_name',
				'label' => _T('webmanifest:configurer_short_name_label'),
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'description',
				'label' => _T('webmanifest:configurer_description_label'),
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'start_url',
				'label' => _T('webmanifest:configurer_start_url_label'),
				'defaut' => $GLOBALS['meta']['adresse_site'],
			),
		),
		array(
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'display',
				'label' => _T('webmanifest:configurer_display_label'),
				'explication' => _T('webmanifest:configurer_display_explication'),
				'data' => array(
					'fullscreen' => _T('webmanifest:configurer_display_choix_fullscreen_label'),
					'standalone' => _T('webmanifest:configurer_display_choix_standalone_label'),
					'minimal-ui' => _T('webmanifest:configurer_display_choix_minimal-ui_label'),
					'browser' => _T('webmanifest:configurer_display_choix_browser_label'),
				),
				'defaut' => 'minimal-ui',
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'theme_color',
				'type' => 'color',
				'label' => _T('webmanifest:configurer_theme_color_label'),
				'defaut' => '#ffffff',
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'background_color',
				'type' => 'color',
				'label' => _T('webmanifest:configurer_background_color_label'),
				'defaut' => '#ffffff',
			),
		),
	);
	
	return $saisies;
}

function formulaires_configurer_webmanifest_traiter_dist() {
	include_spip('inc/cvt_configurer');
	include_spip('inc/flock');
	include_spip('inc/documents');
	include_spip('inc/config');
	$retours = array();
	
	// On garde en mémoire l'existant (l'API vide sinon)
	if ($fichier = lire_config('webmanifest/icon')) {
		set_request('icon', $fichier);
	}
	
	// On enregistre la nouvelle configuration
	$trace = cvtconf_formulaires_configurer_enregistre('configurer_webmanifest', array());
	
	// On s'occupe du nouveau fichier s'il y a
	if (isset($_FILES['icon'])) {
		// S'il y avait déjà un fichier
		if ($fichier) {
			supprimer_fichier(_DIR_IMG.$fichier);
		}
		
		$repertoire = sous_repertoire(_DIR_IMG . 'configurer/');
		$repertoire = sous_repertoire($repertoire . 'webmanifest/');
		if ($chemin = deplacer_fichier_upload($_FILES['icon']['tmp_name'], $repertoire.$_FILES['icon']['name'])) {
			ecrire_config('webmanifest/icon', 'configurer/webmanifest/'.$_FILES['icon']['name']);
		}
	}
}
